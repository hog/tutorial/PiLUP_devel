#####
# Specify minimum CMake install

cmake_minimum_required(VERSION 2.8)

#####
# Project Name

project(PiLUP_SW C CXX)

#####
# Set the output folder where the program will be created
set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/bin)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})
set(LIBRARY_OUTPUT_PATH ${CMAKE_BINARY_DIR}/lib)

#####
# Included Folders
include_directories("${PROJECT_SOURCE_DIR}/src")

# add_library([SUBDIRECTORY_NAME] [FILENAME])

add_executable(ExampleExecutableName ${PROJECT_SOURCE_DIR}/src/ExampleFunction.cpp, PLL_Filter $PROJECT_SOURCE_DIR}/src/PLL_fitter.C)
