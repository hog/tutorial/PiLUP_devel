-------------------------------------------
-- Frans Schreuder (Nikhef)
-- June 2017
-- package file for FMUserExample
-- 
-------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.numeric_std.all;

package FMTransceiverPackage is
	type slv20_array		is array (natural range <>) of std_logic_vector(19 downto 0);
	type slv40_array		is array (natural range <>) of std_logic_vector(39 downto 0);
	type slv32_array		is array (natural range <>) of std_logic_vector(31 downto 0);
	type slv34_array		is array (natural range <>) of std_logic_vector(33 downto 0);
	type slv84_array		is array (natural range <>) of std_logic_vector(83 downto 0);
	type slv2_array			is array (natural range <>) of std_logic_vector(1 downto 0);
	type slv4_array			is array (natural range <>) of std_logic_vector(3 downto 0);
	type slv26_array		is array (natural range <>) of std_logic_vector(25 downto 0);
	type slv64_array		is array (natural range <>) of std_logic_vector(63 downto 0);
	type slv8_array			is array (natural range <>) of std_logic_vector(7 downto 0);
end FMTransceiverPackage;
