library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
--use work.I2C.all;
use work.FMTransceiverPackage.all;

entity RD53_Emulator_wrapper is
port (
	--reset
	rst			: in std_logic;
	--clock signals
	clk160_in	: in std_logic;
	clk160_out	: out std_logic;
	-- TTC signal
	ttc_in		: in std_logic;
	
	-- control signals
	chip_id		: in std_logic_vector(3 downto 0);
	n_hits_in	: in std_logic_vector(7 downto 0);
	-- output signals
	RD53_data	: out slv64_array(3 downto 0);
	RD53_header	: out std_logic_vector(1 downto 0)
);
end entity RD53_Emulator_wrapper;


architecture structure of RD53_Emulator_wrapper is
----------------------------------------------------------
--                                                      --
--                  Signal Declaration                  --
--                                                      --
----------------------------------------------------------
	signal clk40, clk80, clk160, clk320, clk640						: std_logic;
	signal ttc_i													: std_logic;
	signal valid_i, data_out_valid_i								: std_logic;
	signal data_i													: std_logic_vector(15 downto 0);
	signal aurora_data_i											: slv64_array(3 downto 0);
	signal aurora_head_i											: std_logic_vector(1 downto 0);
	signal ECR_i, BCR_i, pulse_i, cal_i, wrreg_i, rdreg_i, sync_i 	: std_logic;
	
----------------------------------------------------------
--                                                      --
--                  Module Declaration                  --
--                                                      --
----------------------------------------------------------	
	component clk_wiz_RD53 is
	Port (
		-- Clock out ports
		clk40_out		: out std_logic;
		clk160_out		: out std_logic;
		clk320_out		: out std_logic;
		clk640_out		: out std_logic;
		clk80_out		: out std_logic;
		-- Status and control signals
		reset			: in std_logic;
		locked			: out std_logic;
		-- Clock in ports
		clk_in1			: in std_logic
	); 
	end component clk_wiz_RD53;
	
	COMPONENT TTC_sync_fifo
	PORT (
		rst : IN STD_LOGIC;
		wr_clk : IN STD_LOGIC;
		rd_clk : IN STD_LOGIC;
		din : IN STD_LOGIC;
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC;
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC
	);
	END COMPONENT;
	
	component ttc_top is
	Port (
		clk640			: in std_logic; 
		clk320			: in std_logic; 
		clk160			: in std_logic; 
		rst				: in std_logic;
		datain			: in std_logic;	
		valid			: out std_logic;
		data			: out std_logic_vector(15 downto 0);
		recovered_clk	: out std_logic;
		phase			: out std_logic_vector(1 downto 0)
	);
	end component ttc_top;
	
	
	component chip_output is
	Port(
		rst				: in std_logic;
		clk160			: in std_logic;
		clk40			: in std_logic;
		clk80			: in std_logic;
		word_valid		: in std_logic;
		data_in			: in std_logic_vector(15 downto 0);
		chip_id			: in std_logic_vector(3 downto 0);
		n_hits_in		: in std_logic_vector(7 downto 0);
		aurora_data_out	: out slv64_array(3 downto 0);
		aurora_head_out	: out std_logic_vector(1 downto 0);
		trig_out		: out std_logic;
		fifo_full		: out std_logic;
		TT_full			: out std_logic;
		TT_empty		: out std_logic;
		ECR_o			: out std_logic;
		BCR_o			: out std_logic;
		pulse_o			: out std_logic;
		cal_o			: out std_logic;
		wrreg_o			: out std_logic;
		rdreg_o			: out std_logic;
		sync_o			: out std_logic
	);
	end component chip_output;	
	
	-- DEBUGGING
	COMPONENT RD53A_ila	
	PORT (
		clk : IN STD_LOGIC;	
		probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		probe3 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		probe4 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
		probe5 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
		probe6 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
		probe7 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
		probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
	);
	END COMPONENT  ;
	
begin
----------------------------------------------------------
--                                                      --
--                 Module Implementation                --
--                                                      --
----------------------------------------------------------
	clk160_out		<= clk160;
	RD53_data		<= aurora_data_i;
	RD53_header		<= aurora_head_i;

	--RD53 clock wizard
	RD53_clock: clk_wiz_RD53
	Port Map(
		-- Clock out ports
		clk40_out		=> clk40,
		clk160_out		=> clk160,
		clk320_out		=> clk320,
		clk640_out		=> clk640,
		clk80_out		=> clk80,
		reset			=> rst,
		locked			=> open,
		-- Clock in ports
		clk_in1			=> clk160_in
	); 

	TTC_sycn_fifo_impl: TTC_sync_fifo
	Port Map(
		rst				=> rst,
		wr_clk			=> clk160_in,
		rd_clk			=> clk160,
		din				=> TTC_in,
		wr_en			=> '1',
		rd_en			=> '1',
		dout			=> TTC_i,
		full			=> open,
		empty			=> open
	);

	ttc_decoder: ttc_top
	Port Map (
		clk640			=> clk640,
		clk320			=> clk320,
		clk160			=> clk160,
		rst				=> rst,
		datain			=> ttc_i,
		valid			=> valid_i,
		data			=> data_i,
		recovered_clk	=> open,
		phase			=> open
	);
	
	
	chip_output_inst: chip_output
	Port Map(
		rst				=> rst,
		clk160			=> clk160,
		clk40			=> clk40,
		clk80			=> clk80,
		word_valid		=> valid_i,
		data_in			=> data_i,
		chip_id			=> chip_id,
		n_hits_in		=> n_hits_in,
		aurora_data_out	=> aurora_data_i,
		aurora_head_out	=> aurora_head_i,
		trig_out		=> open,
		fifo_full		=> open,
		TT_full			=> open,
		TT_empty		=> open,
		ECR_o			=> ECR_i,
		BCR_o			=> BCR_i,
		pulse_o			=> pulse_i,
		cal_o			=> cal_i,
		wrreg_o			=> wrreg_i,
		rdreg_o			=> rdreg_i,
		sync_o			=> sync_i
	);
	
		-- debugging
	rd53_debug : RD53A_ila	
	PORT MAP (
		clk 				=> clk160,	
		probe0(0)			=> TTC_i,
		probe1				=> data_i,
		probe2(0)			=> valid_i,
		probe3				=> aurora_head_i,
		probe4				=> aurora_data_i(0),
		probe5				=> aurora_data_i(1),
		probe6				=> aurora_data_i(2),
		probe7				=> aurora_data_i(3),
		probe8(0)			=> ECR_i,
		probe9(0)			=> wrreg_i,
		probe10(0)			=> rdreg_i,
		probe11(0)			=> sync_i
	);
	
	
end architecture structure ; -- of RD53_Emulator_wrapper
