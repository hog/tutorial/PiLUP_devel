----------------------------------------------------------------------------------
-- Company: INFN & University of Bologna
-- Developer: Nico Giangiacomi
-- 
-- Create Date: 12/12/2017 05:28:22 PM
-- Design Name: 
-- Module Name: data_generator - Behavioral
-- Project Name: Protocol_Converter
-- Target Devices: KC705, PiLUP, ZC706, KCU105
-- Tool Versions: 
-- Description:  This module packs data in RD53A output compatible format
-- 4 lane 64+2 bits at 1.28 Gbps each
--
-- The output protocol is up-to-data with RD53A Manual 02/2018
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.FMTransceiverPackage.all;

entity cmd_out is
Port (
	rst				: in std_logic;
	clk20			: in std_logic;
	clk40			: in std_logic;
	clk80			: in std_logic;
	clk160			: in std_logic;
	hit_in			: in std_logic_vector(31 downto 0);
	hit_valid		: in std_logic;
	reg_data_wr		: in std_logic;
	reg_data		: in std_logic_vector(15 downto 0);
	reg_addr		: in std_logic_vector(8 downto 0);
	cmd_full		: out std_logic;
	autoreg_data_A0	: in std_logic_vector(25 downto 0);
	autoreg_data_A1	: in std_logic_vector(25 downto 0);
	autoreg_data_A2	: in std_logic_vector(25 downto 0);
	autoreg_data_A3	: in std_logic_vector(25 downto 0);	
	autoreg_data_B0	: in std_logic_vector(25 downto 0);
	autoreg_data_B1	: in std_logic_vector(25 downto 0);
	autoreg_data_B2	: in std_logic_vector(25 downto 0);
	autoreg_data_B3	: in std_logic_vector(25 downto 0);
	
	aurora_head_out	: out std_logic_vector(1 downto 0);
	aurora_data_out0: out std_logic_vector(63 downto 0);
	aurora_data_out1: out std_logic_vector(63 downto 0);
	aurora_data_out2: out std_logic_vector(63 downto 0);
	aurora_data_out3: out std_logic_vector(63 downto 0);
	
	n_data_frames	: in integer:= 48
);
end cmd_out;

architecture Behavioral of cmd_out is

--########################################################################
--##																	##
--##						Signals declaration							##
--##																	##
--########################################################################
	signal hit_fifo_cnt						: std_logic_vector(10 downto 0);
	signal hit_i							: std_logic_vector(31 downto 0);
	signal hit_valid_i, hitFifo_empty		: std_logic;
	signal hit_ren							: std_logic;

	signal reg_dat_add_in					: std_logic_vector(24 downto 0);
	signal reg_dat_add_i					: std_logic_vector(24 downto 0);
	signal reg_fifo_cnt						: std_logic_vector(10 downto 0);
	signal reg_data_wr_i, reg_data_wr_ii	: std_logic;
	signal reg_valid_i, regFifo_empty		: std_logic;
	signal reg_data_ren						: std_logic;
	
	signal Aurora_head						: std_logic_vector(1 downto 0);
	signal frame_cnt						: integer range 0 to 100;
	signal lane								: integer range 0 to 3;
	type state_type							IS (boot, hit_0, hit_1, reg_0, reg_1); 
	signal state							: state_type := boot;
	
	signal RD53_data_i						: std_logic_vector(257 downto 0):=(others => '0');
	
	signal status							: std_logic_vector(3 downto 0):= x"0";
	signal regCmdFound						: std_logic_vector(1 downto 0);
	
	signal autoreg_dataA, autoreg_dataB		: slv26_array(3 downto 0);
	
	
--########################################################################
--##																	##
--##						Component declaration						##
--##																	##
--########################################################################
						
	COMPONENT data_fifo
	PORT (
		clk 		: IN STD_LOGIC;
		srst 		: IN STD_LOGIC;
		din 		: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en 		: IN STD_LOGIC;
		rd_en 		: IN STD_LOGIC;
		dout 		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		full 		: OUT STD_LOGIC;
		empty 		: OUT STD_LOGIC;
		valid 		: OUT STD_LOGIC;
		data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
		prog_full 	: OUT STD_LOGIC
	);
	END COMPONENT;

	COMPONENT reg_fifo
	PORT (
		clk 		: IN STD_LOGIC;
		srst 		: IN STD_LOGIC;
		din 		: IN STD_LOGIC_VECTOR(24 DOWNTO 0);
		wr_en 		: IN STD_LOGIC;
		rd_en 		: IN STD_LOGIC;
		dout 		: OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
		full 		: OUT STD_LOGIC;
		empty 		: OUT STD_LOGIC;
		valid 		: OUT STD_LOGIC;
		data_count 	: OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
		prog_full 	: OUT STD_LOGIC
	);
	END COMPONENT;

begin
	
	autoreg_dataA(0)	<= autoreg_data_A0;
	autoreg_dataA(1)	<= autoreg_data_A1;
	autoreg_dataA(2)	<= autoreg_data_A2;
	autoreg_dataA(3)	<= autoreg_data_A3;
	autoreg_dataB(0)	<= autoreg_data_B0;
	autoreg_dataB(1)	<= autoreg_data_B1;
	autoreg_dataB(2)	<= autoreg_data_B2;
	autoreg_dataB(3)	<= autoreg_data_B3;
--########################################################################
--##																	##
--##						Component implementation					##
--##																	##
--########################################################################
	hit_fifo_inst: data_fifo
	Port Map (
		clk			=> clk160,
		srst		=> rst,
		din			=> hit_in,
		wr_en		=> hit_valid,
		rd_en		=> hit_ren,
		dout		=> hit_i,
		full		=> open,
		empty		=> hitFifo_empty,
		valid		=> hit_valid_i,
		data_count	=> hit_fifo_cnt,
		prog_full	=> open
	);
	
	reg_fifo_inst: reg_fifo
	Port Map (
		clk			=> clk160,
		srst		=> rst,
		din			=> reg_dat_add_in,
		wr_en		=> reg_data_wr_ii,
		rd_en		=> reg_data_ren,
		dout		=> reg_dat_add_i,
		full		=> open,
		empty		=> regFifo_empty,
		valid		=> reg_valid_i,
		data_count	=> reg_fifo_cnt,
		prog_full	=> cmd_full
	);


--########################################################################
--##																	##
--##						Process implementation						##
--##																	##
--########################################################################

	wr_en_160MHz: process(clk160)
	begin
		if(rising_edge(clk160)) then
			reg_data_wr_i		<= reg_data_wr;
			reg_dat_add_in		<= reg_addr & reg_data;
			if(reg_data_wr= '1') and (reg_data_wr_i= '0') then
				reg_data_wr_ii <= '1';
			else
				reg_data_wr_ii <= '0';
			end if;
		end if;
	end process wr_en_160MHz;

	output_generator: process(rst, clk160)
	begin
		if(rst = '1') then
			frame_cnt		<= 0;
			lane			<= 0;
			hit_ren			<= '0';
			reg_data_ren	<= '0';
			state			<= boot;
		elsif(rising_edge(clk160)) then
			hit_ren			<= '0';
			reg_data_ren	<= '0';
			
			case state is
				when boot =>
					lane			<= 0;
					if (unsigned(hit_fifo_cnt) > 7) then
						hit_ren								<= '1';
					end if;
					state		<= hit_0;
				when hit_0 =>
					if(lane = 0) then
						aurora_head_out		<= RD53_data_i(257 downto 256);
						aurora_data_out3	<= RD53_data_i(255 downto 192);
						aurora_data_out2	<= RD53_data_i(191 downto 128);
						aurora_data_out1	<= RD53_data_i(127 downto  64);
						aurora_data_out0	<= RD53_data_i( 63 downto   0); 
					end if;
					if(hit_ren = '1') then
						RD53_data_i(63+64*lane downto 32+64*lane)	<= hit_i;
						hit_ren										<= '1';
						RD53_data_i(257 downto 256)					<= "01";
					elsif (hitFifo_empty = '0') then
						RD53_data_i(257 downto 256)					<= "10";
						RD53_data_i(63+64*lane downto 32+64*lane)	<= x"1E040000";
						hit_ren										<= '1';
					else
						RD53_data_i(257 downto 256)					<= "10";
						RD53_data_i(63+64*lane downto 32+64*lane)	<= x"1E000000";
						hit_ren										<= '0';
					end if;	 
					state		<= hit_1;
				when hit_1 =>
					lane		<= lane + 1;
					if(hit_ren= '1') then
						RD53_data_i(31+64*lane downto 64*lane)		<= hit_i;
					else
						RD53_data_i(31+64*lane downto 64*lane)		<= x"00000000";
					end if;
					if(lane = 3) then
						lane		<= 0;
						frame_cnt	<= frame_cnt + 1;
						if(frame_cnt = (n_data_frames-1)) then
							state		<= reg_0;
							if(regFifo_empty = '0') then
								reg_data_ren				<= '1';
							end if;
						else
							state		<= hit_0;
							if (unsigned(hit_fifo_cnt) > 7) then
								hit_ren				<= '1';
							end if;
						end if;
					else
						state		<= hit_0;
						if (RD53_data_i(257 downto 256) = "01") then
							hit_ren				<= '1';
						end if;
					end if;
					
				when reg_0 =>
					if(lane = 0) then
						aurora_head_out		<= RD53_data_i(257 downto 256);
						aurora_data_out3	<= RD53_data_i(255 downto 192);
						aurora_data_out2	<= RD53_data_i(191 downto 128);
						aurora_data_out1	<= RD53_data_i(127 downto  64);
						aurora_data_out0	<= RD53_data_i( 63 downto   0); 
					end if;
					if(reg_data_ren = '1') then
						regCmdFound(1)								<= '1';
						RD53_data_i(51+64*lane downto 26+64*lane)	<= '1' & reg_dat_add_i;
					else 
						regCmdFound(1)								<= '0';
						RD53_data_i(51+64*lane downto 26+64*lane)	<= autoreg_dataA(lane);
					end if;
					
					if(unsigned(reg_fifo_cnt) > 1) and (lane = 0) then
						reg_data_ren								<= '1';
					end if;
					state	<= reg_1;
					
				when reg_1 =>
					lane		<= lane + 1;
					if(reg_data_ren= '1') then
						RD53_data_i(25+64*lane downto 64*lane)		<= '1' & reg_dat_add_i;
						regCmdFound(0)								<= '1';
					else
						RD53_data_i(25+64*lane downto 64*lane)		<= autoreg_dataB(lane);
						regCmdFound(0)								<= '0';
					end if;
					
					if(regCmdFound(1) = '0') and (reg_data_ren = '0') then RD53_data_i(63+64*lane downto 56+64*lane) <= x"B4";
					elsif(regCmdFound(1) = '0') and (reg_data_ren = '1') then RD53_data_i(63+64*lane downto 56+64*lane) <= x"55";
					elsif(regCmdFound(1) = '1') and (reg_data_ren = '0') then RD53_data_i(63+64*lane downto 56+64*lane) <= x"99";
					elsif(regCmdFound(1) = '1') and (reg_data_ren = '1') then RD53_data_i(63+64*lane downto 56+64*lane) <= x"D2";
					end if;	
					
					RD53_data_i(55+64*lane downto 52+64*lane) <= status;
					
					if(lane = 3) then
						lane		<= 0;
						frame_cnt	<= 0;
						
						state		<= hit_0;
						if (unsigned(hit_fifo_cnt) > 7) then
							hit_ren					<= '1';
						end if;
						
					else
						state		<= reg_0;
					end if;
			end case;
		end if;
	end process output_generator; 



end Behavioral;
