----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/12/2017 05:28:22 PM
-- Design Name: 
-- Module Name: data_generator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_generator is
Port (
	rst				: in std_logic;
	clk40			: in std_logic;
	clk80			: in std_logic;
	clk160			: in std_logic;
	triggerClump	: in std_logic_vector(3 downto 0);
	trig_tag		: in std_logic_vector(4 downto 0);
	BCID_cnt		: in std_logic_vector(15 downto 0);
	trig_valid		: in std_logic;
	data_out		: out std_logic_vector(31 downto 0);
	data_valid		: out std_logic;
	n_hits			: in integer
);
end data_generator;

architecture Behavioral of data_generator is

--########################################################################
--##																	##
--##						Signals declaration							##
--##																	##
--########################################################################
type state_type is (idle, process_trigger);  -- Define the states

signal hit_cnt						: integer := 0;

signal TT_state, data_state			: state_type := idle;
signal TT_in, TT_out				: std_logic_vector(24 downto 0);
signal TT_out_valid					: std_logic;
signal TT_cnt						: std_logic_vector(4 downto 0);
signal TT_wr_en						: std_logic := '0'; 
signal TT_rd_en, TT_rd_en_i			: std_logic := '0';
signal triggerClump_i				: std_logic_vector(3 downto 0);
signal clk_cnt						: integer;

signal data_out_valid, data_valid_i	: std_logic := '0';
signal data_full, data_empty		: std_logic;


signal load, step					: std_logic;
signal col_data						: std_logic_vector(6 downto 0);
signal col_data_valid				: std_logic;
signal row_data						: std_logic_vector(8 downto 0);
signal row_data_valid				: std_logic;
signal TOT_data						: std_logic_vector(15 downto 0);
signal TOT_data_valid				: std_logic;

--########################################################################
--##																	##
--##						Component declaration						##
--##																	##
--########################################################################
					
component TT_fifo 
PORT (
	clk : IN STD_LOGIC;
	rst : IN STD_LOGIC;
	din : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
	wr_en : IN STD_LOGIC;
	rd_en : IN STD_LOGIC;
	dout : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
	full : OUT STD_LOGIC;
	empty : OUT STD_LOGIC;
	valid : OUT STD_LOGIC;
	data_count : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
);
END component TT_fifo;

component updatedPRNG  
GENERIC(
	width 		: integer := 32
);
PORT (
	clk			: IN STD_LOGIC; 
	reset		: IN STD_LOGIC; 
	load		: IN STD_LOGIC;
	step		: IN STD_LOGIC;
	seed		: IN STD_LOGIC_VECTOR(width-1 downto 0);
	dout		: OUT STD_LOGIC_VECTOR(width-1 downto 0);
	new_data	: OUT STD_LOGIC
);
END component updatedPRNG;

begin

--########################################################################
--##																	##
--##						Component implementation					##
--##																	##
--########################################################################
					
TT_fifo_inst : TT_Fifo 
PORT Map(
	clk 			=> clk80, 
	rst 			=> rst,
	din				=> TT_in,
	wr_en 			=> TT_wr_en,
	rd_en 			=> TT_rd_en,
	dout 			=> TT_out,
	full 			=> open,
	empty 			=> open,
	valid 			=> TT_out_valid,
	data_count		=> TT_cnt
);


col_generator: updatedPRNG  
Generic Map
(
	width 		=> 7
)
Port map
(
	clk			=> clk160,
	reset		=> rst, 
	load		=> load,
	step		=> step,
	seed		=> BCID_cnt(6 downto 0),
	dout		=> col_data,
	new_data	=> col_data_valid
);

row_generator: updatedPRNG  
Generic Map
(
	width 		=> 9
)
Port map
(
	clk			=> clk160,
	reset		=> rst, 
	load		=> load,
	step		=> step,
	seed		=> BCID_cnt(12 downto 4),
	dout		=> row_data,
	new_data	=> row_data_valid
);

TOT_generator: updatedPRNG  
Generic Map
(
	width 		=> 16
)
Port map
(
	clk			=> clk160,
	reset		=> rst, 
	load		=> load,
	step		=> step,
	seed		=> BCID_cnt(15 downto 0),
	dout		=> TOT_data,
	new_data	=> TOT_data_valid
);


--########################################################################
--##																	##
--##						Processes implementation					##
--##																	##
--########################################################################
fill_TT_fifo: process(rst, clk80)
begin
	if(rst = '1') then
		TT_in		<= (others => '0');
		TT_wr_en	<= '0';
		clk_cnt		<= 0;
		TT_state	<= idle; 
	elsif(rising_edge (clk80)) then
		case TT_state is
			when idle =>
				TT_in		<= (others => '0');
				TT_wr_en	<= '0';
				clk_cnt		<= 0;
				if(trig_valid = '1') then
					TT_state		<= process_trigger;
					triggerClump_i	<= triggerClump;
				end if; 
			when process_trigger =>
				clk_cnt		<= clk_cnt + 1;
				if(clk_cnt = 0) and (triggerClump_i(0) = '1') then
					TT_in		<= TT_cnt & trig_tag & BCID_cnt(15 downto 1);
					TT_wr_en	<= '1';
				elsif(clk_cnt = 2) and (triggerClump_i(1) = '1') then
					TT_in		<= TT_cnt & trig_tag & BCID_cnt(15 downto 1);
					TT_wr_en	<= '1';
				elsif(clk_cnt = 4) and (triggerClump_i(2) = '1') then
					TT_in		<= TT_cnt & trig_tag & BCID_cnt(15 downto 1);
					TT_wr_en	<= '1';
				elsif(clk_cnt = 6) and (triggerClump_i(3) = '1') then
					TT_in		<= TT_cnt & trig_tag & BCID_cnt(15 downto 1);
					TT_wr_en	<= '1';
				elsif(clk_cnt = 7) then
					TT_state	<= idle;
					TT_wr_en	<= '0';
				else
					TT_wr_en	<= '0';
				end if;
		end case;
	end if;
end process fill_TT_fifo;


fill_data_fifo: process(rst, clk160)
begin
	if(rst = '1') then
		TT_rd_en		<= '0';
		data_valid		<= '0';
		hit_cnt			<= 0;
		load			<= '0';
		step			<= '0';
		data_state		<= idle;
	elsif(rising_edge(clk160)) then
		TT_rd_en_i		<= TT_rd_en;
		case data_state is
			when idle =>
				hit_cnt			<= 0;
				data_valid		<= '0';
				load			<= '0';
				step			<= '0';
				TT_rd_en	<= not TT_rd_en_i and not data_full;
				if(TT_out_valid = '1') then
					TT_rd_en		<= '0';
					data_valid		<= '1';
					data_out		<= "0000001" & TT_out;
					--load			<= '1';
					data_state		<= process_trigger;
				end if;
			when process_trigger =>
				load			<= '0';
				TT_rd_en		<= '0';
				data_valid		<= '0';
				if (data_full	<= '0') then
					step			<= '1';
				end if;
				if(col_data_valid = '1' and row_data_valid = '1' and TOT_data_valid = '1') then
					data_valid		<= '1';
					data_out			<= col_data(5 downto 0) & '0' & row_data & TOT_data;
					hit_cnt			<= hit_cnt + 1;
				end if;
				if (hit_cnt = n_hits) then
					data_valid		<= '0';
					data_state		<= idle;
				end if;
		end case;
	end if;
end process fill_data_fifo;


end Behavioral;
