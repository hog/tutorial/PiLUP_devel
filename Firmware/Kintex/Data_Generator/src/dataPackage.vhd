library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.numeric_std.all;

package dataPackage is
	type slv2_array			is array (natural range <>) of std_logic_vector(1 downto 0);
	type slv64_array		is array (natural range <>) of std_logic_vector(63 downto 0);
	type slv32_array		is array (natural range <>) of std_logic_vector(31 downto 0);
end dataPackage;
