# Master to Slave chip2chip CLK
set_property IOSTANDARD LVDS_25 [get_ports KZ_BUS_CLK_*]
set_property PACKAGE_PIN Y16 [get_ports KZ_BUS_CLK_N]
set_property PACKAGE_PIN W16 [get_ports KZ_BUS_CLK_P]


# Differential AXI_C2C_IN (slave to master chip2chip)
set_property IOSTANDARD LVDS_25 [get_ports AXI_C2C_IN_*]
#set_property PACKAGE_PIN T22  [get_ports AXI_C2C_IN_P[0]]
#set_property PACKAGE_PIN U22  [get_ports AXI_C2C_IN_N[0]]
set_property PACKAGE_PIN V22  [get_ports AXI_C2C_IN_P[0]]
set_property PACKAGE_PIN W22  [get_ports AXI_C2C_IN_N[0]]
set_property PACKAGE_PIN W20  [get_ports AXI_C2C_IN_P[1]]
set_property PACKAGE_PIN W21  [get_ports AXI_C2C_IN_N[1]]
set_property PACKAGE_PIN U20  [get_ports AXI_C2C_IN_P[2]]
set_property PACKAGE_PIN V20  [get_ports AXI_C2C_IN_N[2]]
set_property PACKAGE_PIN V18  [get_ports AXI_C2C_IN_P[3]]
set_property PACKAGE_PIN V19  [get_ports AXI_C2C_IN_N[3]]
set_property PACKAGE_PIN AA22 [get_ports AXI_C2C_IN_P[4]]
set_property PACKAGE_PIN AB22 [get_ports AXI_C2C_IN_N[4]]
set_property PACKAGE_PIN AA21 [get_ports AXI_C2C_IN_P[5]]
set_property PACKAGE_PIN AB21 [get_ports AXI_C2C_IN_N[5]]
set_property PACKAGE_PIN Y20  [get_ports AXI_C2C_IN_P[6]]
set_property PACKAGE_PIN Y21  [get_ports AXI_C2C_IN_N[6]]
set_property PACKAGE_PIN AB19 [get_ports AXI_C2C_IN_P[7]]
set_property PACKAGE_PIN AB20 [get_ports AXI_C2C_IN_N[7]]
set_property PACKAGE_PIN AA19 [get_ports AXI_C2C_IN_N[8]]
set_property PACKAGE_PIN Y19  [get_ports AXI_C2C_IN_P[8]]


# Slave to Master CLK
set_property IOSTANDARD LVDS_25 [get_ports KZ_CLK_IN_*]
set_property DIFF_TERM true [get_ports KZ_CLK_IN_*]
set_property PACKAGE_PIN AA18 [get_ports KZ_CLK_IN_N]
set_property PACKAGE_PIN Y18 [get_ports KZ_CLK_IN_P]


# Differential AXI_C2C_OUT (master to slave chip2chip)
set_property IOSTANDARD LVDS_25 [get_ports AXI_C2C_OUT_*]
set_property DIFF_TERM true [get_ports AXI_C2C_OUT_*]
#set_property PACKAGE_PIN T21 [get_ports {AXI_C2C_IN_P[0]}]
#set_property PACKAGE_PIN U21 [get_ports {AXI_C2C_IN_N[0]}]
set_property PACKAGE_PIN W17  [get_ports AXI_C2C_OUT_P[0]]
set_property PACKAGE_PIN W18  [get_ports AXI_C2C_OUT_N[0]]
set_property PACKAGE_PIN U15  [get_ports AXI_C2C_OUT_P[1]]
set_property PACKAGE_PIN U16  [get_ports AXI_C2C_OUT_N[1]]
set_property PACKAGE_PIN U17  [get_ports AXI_C2C_OUT_P[2]]
set_property PACKAGE_PIN V17  [get_ports AXI_C2C_OUT_N[2]]
set_property PACKAGE_PIN AA17 [get_ports AXI_C2C_OUT_P[3]]
set_property PACKAGE_PIN AB17 [get_ports AXI_C2C_OUT_N[3]]
set_property PACKAGE_PIN AA16 [get_ports AXI_C2C_OUT_P[4]]
set_property PACKAGE_PIN AB16 [get_ports AXI_C2C_OUT_N[4]]
set_property PACKAGE_PIN V14  [get_ports AXI_C2C_OUT_P[5]]
set_property PACKAGE_PIN V15  [get_ports AXI_C2C_OUT_N[5]]
set_property PACKAGE_PIN V13  [get_ports AXI_C2C_OUT_P[6]]
set_property PACKAGE_PIN W13  [get_ports AXI_C2C_OUT_N[6]]
set_property PACKAGE_PIN W15  [get_ports AXI_C2C_OUT_P[7]]
set_property PACKAGE_PIN Y15  [get_ports AXI_C2C_OUT_N[7]]
set_property PACKAGE_PIN AA14 [get_ports AXI_C2C_OUT_N[8]]
set_property PACKAGE_PIN Y14  [get_ports AXI_C2C_OUT_P[8]]

# master to slave reset
set_property IOSTANDARD LVCMOS25 [get_ports slave_rst]
set_property PACKAGE_PIN U19 [get_ports slave_rst]
